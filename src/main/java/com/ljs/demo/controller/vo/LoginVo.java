package com.ljs.demo.controller.vo;

import com.ljs.demo.entity.User;

import java.io.Serializable;

/**
 * @author LiJinSong
 * @date 2021-01-28
 */
public class LoginVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private User user;

    private String token;

    public LoginVo() {
    }

    public LoginVo(User user, String token) {
        this.user = user;
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoginVo{" +
                "user=" + user +
                ", token='" + token + '\'' +
                '}';
    }
}
