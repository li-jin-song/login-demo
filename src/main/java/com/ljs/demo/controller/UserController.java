package com.ljs.demo.controller;

import com.ljs.demo.controller.model.Result;
import com.ljs.demo.controller.vo.LoginVo;
import com.ljs.demo.entity.User;
import com.ljs.demo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LiJinSong
 * @date 2021-01-27
 */
@RestController
@RequestMapping("/user")
@Api("用户API")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/add")
    @ApiOperation("添加用户")
    public int add(@RequestBody User user){
        return userService.add(user);
    }

    @PostMapping("/login")
    @ApiOperation("用户登录")
    public Result<LoginVo> login(@RequestBody User user){
        return userService.login(user);
    }
}
