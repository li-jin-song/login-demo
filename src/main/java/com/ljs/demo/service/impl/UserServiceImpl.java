package com.ljs.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ljs.demo.controller.model.Result;
import com.ljs.demo.controller.vo.LoginVo;
import com.ljs.demo.dao.UserDao;
import com.ljs.demo.entity.User;
import com.ljs.demo.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @author LiJinSong
 * @date 2021-01-27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public int add(User user) {
        if(StringUtils.isNotBlank(user.getLoginName()) && StringUtils.isNotBlank(user.getPassword())){
            return userDao.insert(user);
        }
        return 0;
    }

    @Override
    public Result<LoginVo> login(User user) {
        if(StringUtils.isBlank(user.getLoginName())){
            return new Result<>(null,400,"账号不能为空!");
        }
        if(StringUtils.isBlank(user.getPassword())){
            return new Result<>(null,400,"密码不能为空!");
        }

        List<User> users = userDao.selectList(new QueryWrapper<User>().eq("login_name",user.getLoginName() ).eq("password", user.getPassword()));
        if(null != users && users.size() > 0 && user.getPassword().equals(users.get(0).getPassword())){

            LoginVo loginVo = new LoginVo(users.get(0),UUID.randomUUID().toString().replace("-",""));

            return new Result<LoginVo>(loginVo,200,"登录成功!");
        }
        return new Result<>(null,400,"密码错误!");
    }
}
