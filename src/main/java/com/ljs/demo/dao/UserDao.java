package com.ljs.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljs.demo.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author LiJinSong
 * @date 2021-01-27
 */
@Repository
public interface UserDao extends BaseMapper<User> {
}
