package com.ljs.demo.service;

import com.ljs.demo.controller.model.Result;
import com.ljs.demo.controller.vo.LoginVo;
import com.ljs.demo.entity.User;

/**
 * @author LiJinSong
 * @date 2021-01-27
 */
public interface UserService {

    int add (User user);

    Result<LoginVo> login(User user);
}
